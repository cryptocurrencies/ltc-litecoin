# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
  echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
  apt-get install -y build-essential libtool autotools-dev libboost-all-dev libdb4.8-dev libminiupnpc-dev libdb4.8++-dev automake pkg-config libssl-dev libevent-dev bsdmainutils git-core python3 && \
  apt-get upgrade -y && \
  apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
# Checkout latest Sourcecode
RUN git clone https://github.com/litecoin-project/litecoin.git /opt/litecoin
# Build All the Dependencies from the build folder
RUN cd /opt/litecoin && \
  ./autogen.sh && \
  ./configure
RUN cd /opt/litecoin && \
  make
ADD https://github.com/kelseyhightower/confd/releases/download/v0.16.0/confd-0.16.0-linux-amd64 /usr/local/bin/confd
COPY confd /etc/confd
COPY start.sh /usr/local/bin/start
RUN chmod +x /usr/local/bin/confd \
  && chmod +x /usr/local/bin/start

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
  echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
  apt-get install -y  libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev libevent-dev && \
  apt-get clean && rm -rf /var/lib/apt/lists/ /tmp/ /var/tmp/*
RUN mkdir /data
COPY --from=build /usr/local/bin/confd /usr/local/bin/confd
COPY --from=build /etc/confd /etc/confd
COPY --from=build /usr/local/bin/start /usr/local/bin/start
COPY --from=build /opt/litecoin/src/litecoind /usr/local/bin/litecoind
COPY --from=build /opt/litecoin/src/litecoin-cli /usr/local/bin/litecoin-cli
RUN chmod +x /usr/local/bin/confd \
  && chmod +x /usr/local/bin/start
VOLUME /data
# ---- Right port is 9332 9333 ---- #
EXPOSE 9334 9333
CMD ["/usr/local/bin/start"]
