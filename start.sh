#!/usr/bin/env bash
set -e

role=${CONTAINER_ROLE:-app}
# Run confd on any kind of container
confd -onetime -backend env

# App Role
if [ "$role" = "app" ]; then

	exec /usr/local/bin/litecoind -datadir=/data -printtoconsole

else
    echo "Could not match the container role \"$role\""
    exit 1
fi
